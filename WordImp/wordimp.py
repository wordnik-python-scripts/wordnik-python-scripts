#! /usr/bin/env python

import wordnik as wn
import random as r


class WordImp(wn.Wordnik):
    def __init__(self, key, usr, passwd, Input, Analysis, Memory, 
                 allow_random=False):
        wn.Wordnik.__init__(self, key, usr, passwd)
        self.Input = Input
        self.Analysis = Analysis
        self.Memory = Memory
        self.allow_random = allow_random

    def get_input(self):
        """Returns the list of words from the Input list."""
        try:
            word_list = self.word_list_get_words(self.Input)
        except ValueError:
            pass

        return self.strip_list(word_list)

    def get_analysis(self):
        """Returns the word in Analysis list. Takes action if more than one."""
        try:
            word_list = self.word_list_get_words(self.Analysis)
        except ValueError:
            pass

        an_list = self.strip_list(word_list)
        if len(an_list) == 0:
            return None
        elif len(an_list) == 1:
            return an_list[0]
        else:
            print "AN list contained more than one word!"
            word = an_list.pop(r.randint(0, len(an_list)-1))
            print "Picked: " + word 
            print "Putting these back in IN list:"
            mem_list = self.get_memory()
            for item in an_list:
                if item not in mem_list:
                    self.put_input(item)
                    self.del_analysis(item)
                    print item

            return word

    def get_memory(self):
        """Returns the list of words from the Memory list."""
        try:
            word_list = self.word_list_get_words(self.Memory)
        except ValueError:
            pass

        return self.strip_list(word_list)

    def put_input(self, word):
        """Adds _word_ to the Input list."""
        try:
            self.word_list_post_words(wordListId=self.Input, body=[word])
        except ValueError:
            pass

        return self.get_input()

    def put_analysis(self, word):
        """Adds _word_ to the Analysis list."""
        try:
            self.word_list_post_words(wordListId=self.Analysis, body=[word])
        except ValueError:
            pass

        return self.get_analysis()

    def put_memory(self, word):
        """Adds _word_ to the Memory list."""
        try:
            self.word_list_post_words(wordListId=self.Memory, body=[word])
        except ValueError:
            pass

        return self.get_memory()

    def del_input(self, word):
        """Delete _word_ from the Input list."""
        try:
            self.word_list_post_delete_words(wordListId=self.Input, body=[word])
        except ValueError:
            pass

        return self.get_input()

    def del_analysis(self, word):
        """Delete _word_ from the Analysis list."""
        try:
            self.word_list_post_delete_words(wordListId=self.Analysis, body=[word])
        except ValueError:
            pass

        return self.get_analysis()

    def del_memory(self, word):
        """Delete _word_ from the Memory list."""
        try:
            self.word_list_post_delete_words(wordListId=self.Memory, body=[word])
        except ValueError:
            pass

        return self.get_memory()
    
    def get_random(self):
        """Get a random word from all o Wordnik. """
        return self.words_get_random_word()['word']

    def strip_list(self, word_list):
        """Strip wordlist from Wordnik API and return only the actual words."""
        stripped_list = []
        for item in word_list:
            stripped_list.append(item[u'word'])
        
        return stripped_list

    def do_cycle(self):
        """Perform a full cycle:
            * get a (random) word from Input.
                - if list is empty, do nothing or pick random word
            * move word from Analysis to Memory.
            * put word from Input in Analysis
        """
        word_list = self.get_input()
        if self.allow_random:
            word_list.append(self.get_random())

        if word_list:
            in_word = word_list[r.randint(0, len(word_list)-1)]
        else:
            in_word = None

        an_word = self.get_analysis()
        if an_word:
            self.put_memory(an_word)
            self.del_analysis(an_word)

        if in_word:
            self.put_analysis(in_word)
            self.del_input(in_word)

        return in_word


            
            
        


        

