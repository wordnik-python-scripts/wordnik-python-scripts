#! /usr/bin/env python

import os
import wordniker as wn

# TODO: comments and docstrings.

script_wd = os.path.dirname(os.path.abspath(__file__))
if os.getcwd() != script_wd:
    os.chdir(script_wd)


with open('myaccount', 'r') as f:
    account = f.readline()
    key, user, passwd = account.split(' ')
    passwd = passwd.rstrip()
    
with open('mygifts', 'r') as f:
    gifts = (f.readline()).rstrip()

with open('myotherlists', 'r') as f:
    other_lists = []
    while 1:
        a_list = f.readline()
        if a_list:
            other_lists.append(a_list.rstrip())
        else:
            break


W = wn.WordNiker(key, user, passwd, gifts, other_lists)



