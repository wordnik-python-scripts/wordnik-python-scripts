#! /usr/bin/env python

import wordnik as wn
import random as r

# TODO: comments and doc-strings

class WordNiker(wn.Wordnik):
    def __init__(self, key, usr, passwd, gifts, other_lists):
        wn.Wordnik.__init__(self, key, usr, passwd)
        self.gifts = gifts
        self.other_lists = other_lists

    def get_gifts(self):
        """Returns the list of words from the Gift list."""
        try:
            word_list = self.word_list_get_words(self.gifts)
        except ValueError:
            pass

        return self.strip_list(word_list)

    def put_gifts(self, word):
        """Adds _word_ to the Gift list."""
        try:
            self.word_list_post_words(wordListId=self.gifts, body=[word])
        except ValueError:
            pass

        return self.get_gifts()

    def del_gifts(self, word):
        """Delete _word_ from the Gift list."""
        try:
            self.word_list_post_delete_words(wordListId=self.gifts, body=[word])
        except ValueError:
            pass

        return self.get_gifts()
    
    def get_list(self, the_list):
        """Returns the _word_list_ of words from the _the_list_."""
        try:
            word_list = self.word_list_get_words(the_list)
        except ValueError:
            pass

        return self.strip_list(word_list)

    def put_list(self, word, the_list):
        """Adds _word_ to the _the_list_."""
        try:
            self.word_list_post_words(wordListId=the_list, body=[word])
        except ValueError:
            pass

        return self.get_list(the_list)

    def del_list(self, word, the_list):
        """Delete _word_ from the _the_list_."""
        try:
            self.word_list_post_delete_words(wordListId=the_list, body=[word])
        except ValueError:
            pass

        return self.get_list(the_list)

    def get_all_lists(self):
        word_lists = []
        for l in self.other_lists:
            word_lists.append(self.get_list(l))
        
        return word_lists

    def get_all_words(self):
        word_lists = self.get_all_lists()
        words = []
        for l in word_lists:
            for w in l:
                if w not in words:
                    words.append(w)

        return words

    def get_random(self):
        return self.words_get_random_word()['word']

    def strip_list(self, word_list):
        stripped_list = []
        for item in word_list:
            stripped_list.append(item[u'word'])
        
        return stripped_list
    
    def thrush_gifts(self):
        words = self.get_all_words()
        gifts = self.get_gifts()
        for w in words:
            if w in gifts:
                self.del_gifts(w)

        return self.get_gifts()

        

